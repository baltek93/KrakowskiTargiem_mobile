package com.example.bartosz.krakowskitarg;

import javax.inject.Scope;

/**
 * Created by Bartosz on 26.03.2017.
 */

@Scope
public @interface HomeActivityScope {
}
