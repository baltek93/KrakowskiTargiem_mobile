package com.example.bartosz.krakowskitarg.addoffer.presenter;

import com.example.bartosz.krakowskitarg.addoffer.usecase.AddOfferUseCase;
import com.example.bartosz.krakowskitarg.addoffer.view.AddOfferView;
import com.example.bartosz.krakowskitarg.base.Presenter;
import com.example.bartosz.krakowskitarg.base.UseCase;

/**
 * Created by Bartosz on 21.05.2017.
 */

public class AddOfferPresenter extends Presenter<AddOfferView> {
    AddOfferUseCase mUseCase;

    public AddOfferPresenter(AddOfferUseCase mUseCase) {
        super(mUseCase);
        this.mUseCase = mUseCase;
    }
}
