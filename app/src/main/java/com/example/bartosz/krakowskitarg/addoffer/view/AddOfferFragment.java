package com.example.bartosz.krakowskitarg.addoffer.view;

import com.example.bartosz.krakowskitarg.MyApplication;
import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.addoffer.presenter.AddOfferPresenter;
import com.example.bartosz.krakowskitarg.base.BaseFragment;
import com.example.bartosz.krakowskitarg.offers.di.DaggerOfferComponent;
import com.example.bartosz.krakowskitarg.offers.di.OfferModule;

import javax.inject.Inject;

/**
 * Created by Bartosz on 21.05.2017.
 */

public class AddOfferFragment extends BaseFragment<AddOfferPresenter, AddOfferView> implements AddOfferView {
    @Inject AddOfferPresenter presenter;
    @Override
    public int getLayoutId() {
        return R.layout.fragment_add_offer;
    }

    @Override
    protected void initInjection() {
        DaggerOfferComponent.builder().myApplicationComponent(MyApplication.get(getActivity()).component()).offerModule(new OfferModule()).build().inject(this);
    }

    @Override
    protected AddOfferPresenter getPresenter() {
        return presenter;
    }
}
