package com.example.bartosz.krakowskitarg.authorization.login.presenter;

import android.content.Intent;

import com.example.bartosz.krakowskitarg.MainActivity;
import com.example.bartosz.krakowskitarg.authorization.login.usecase.AuthorizationUseCase;
import com.example.bartosz.krakowskitarg.authorization.login.view.AuthorizationView;
import com.example.bartosz.krakowskitarg.base.AuthorizationToken;
import com.example.bartosz.krakowskitarg.base.Presenter;
import com.example.bartosz.krakowskitarg.data.model.LoginRequest;
import com.example.bartosz.krakowskitarg.data.model.RegisterRequest;

/**
 * Created by Bartosz on 07.04.2017.
 */
public class AuthorizationPresenter extends Presenter<AuthorizationView> implements AuthorizationUseCase.AuthorizationCallback {

    AuthorizationUseCase useCase;

    public AuthorizationPresenter(AuthorizationUseCase mUseCase) {
        super(mUseCase);
        this.useCase = mUseCase;
    }

    @Override
    public void onViewVisible() {
        super.onViewVisible();

    }

    public void onAuthorization(String login, String password) {
        useCase.authorization(this, new LoginRequest(login, password));
    }

    @Override
    public void saveToken(AuthorizationToken token) {
        useCase.getEditor().putString("TOKEN","Bearer "+token.getId_token()).commit();
        getView().getActivity().startActivity(new Intent(getView().getActivity(), MainActivity.class));
        getView().getActivity().finish();
    }

    @Override
    public void showDialog() {
        getView().showDialog("Zaloguj sie na emaila aby potwierdzić rejestracji a nastepnie zaloguj się.");
    }

    public void onRegister(String username, String password, String email) {
        useCase.register(this, new RegisterRequest(username, password, email));
    }
}
