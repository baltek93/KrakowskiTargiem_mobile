package com.example.bartosz.krakowskitarg.authorization.login.usecase;

import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.base.AuthorizationToken;
import com.example.bartosz.krakowskitarg.data.Repository;
import com.example.bartosz.krakowskitarg.base.UseCase;
import com.example.bartosz.krakowskitarg.data.model.LoginRequest;
import com.example.bartosz.krakowskitarg.data.model.RegisterRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

/**
 * Created by Bartosz on 07.04.2017.
 */

public class AuthorizationUseCase extends UseCase {

    public AuthorizationUseCase(Repository mRepository, Retrofit retrofit, SharedPreferences sharedPreferences) {
        super(mRepository, retrofit,sharedPreferences);
    }

    public Call<AuthorizationToken> getToken(LoginRequest loginRequest) {
        return getRepository().authorization(loginRequest);
    }

    public void authorization(AuthorizationCallback authorizationCallback, LoginRequest loginRequest) {
        mRemoteRepositoryInterface.authorization(loginRequest).enqueue(new Callback<AuthorizationToken>() {
            @Override
            public void onResponse(Call<AuthorizationToken> call, Response<AuthorizationToken> response) {
//                Timber.e(response.body().getId_token());
                authorizationCallback.saveToken(response.body());

            }

            @Override
            public void onFailure(Call<AuthorizationToken> call, Throwable t) {

            }
        });
    }

    public void register(AuthorizationCallback authorizationCallback, RegisterRequest registerRequest) {
        mRemoteRepositoryInterface.register(registerRequest).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code()==200)
                authorizationCallback.showDialog();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    public interface AuthorizationCallback {
        void saveToken(AuthorizationToken token);

        void showDialog();
    }


}
