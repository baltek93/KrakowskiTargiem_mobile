package com.example.bartosz.krakowskitarg.authorization.login.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bartosz.krakowskitarg.MyApplication;
import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.authorization.login.di.AuthorizationModule;
import com.example.bartosz.krakowskitarg.authorization.login.di.DaggerAuthorizationComponent;
import com.example.bartosz.krakowskitarg.authorization.login.presenter.AuthorizationPresenter;
import com.example.bartosz.krakowskitarg.base.BaseFragment;

import javax.inject.Inject;

/**
 * Created by Bartosz on 07.04.2017.
 */

public class AuthorizationFragment extends BaseFragment<AuthorizationPresenter, AuthorizationView> implements AuthorizationView {
    private boolean isSigninScreen = true;
    private TextView tvSignupInvoker;
    private LinearLayout llSignup;
    private TextView tvSigninInvoker;
    private LinearLayout llSignin;
    private Button btnSignup;
    private Button btnSignin;
    private EditText etPassword;
    private EditText etUsernameLogin;
    private EditText etUsernameEnroll;
    private EditText etPasswordEnroll;
    private EditText etEmailEnroll;

    @Inject
    AuthorizationPresenter presenter;
//    @Inject
//    SharedPreferences sharedPreferences;

    @Override
    public int getLayoutId() {
        return R.layout.autorization_fragment;
    }

    @Override
    protected void initInjection() {
        DaggerAuthorizationComponent.builder()
                .authorizationModule(new AuthorizationModule())
                .myApplicationComponent(MyApplication.get(getActivity()).component())
                .build().inject(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvSignupInvoker = (TextView) getActivity().findViewById(R.id.tvSignupInvoker);
        tvSigninInvoker = (TextView) getActivity().findViewById(R.id.tvSigninInvoker);

        btnSignup = (Button) getActivity().findViewById(R.id.btnSignup);
        btnSignin = (Button) getActivity().findViewById(R.id.btnSignin);

        llSignup = (LinearLayout) getActivity().findViewById(R.id.llSignup);
        llSignin = (LinearLayout) getActivity().findViewById(R.id.llSignin);

        etPassword = (EditText) getActivity().findViewById(R.id.password_text);
        etUsernameLogin = (EditText) getActivity().findViewById(R.id.username_text);

        etEmailEnroll = (EditText) getActivity().findViewById(R.id.enroll_email);
        etPasswordEnroll = (EditText) getActivity().findViewById(R.id.enroll_password);
        etUsernameEnroll = (EditText) getActivity().findViewById(R.id.enroll_username);

        tvSignupInvoker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSigninScreen = false;
                showSignupForm();
            }
        });

        tvSigninInvoker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSigninScreen = true;
                showSigninForm();
            }
        });
        showSigninForm();

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation clockwise = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_right_to_left);
                if (isSigninScreen) {
                    btnSignup.startAnimation(clockwise);
                } else {
                    getPresenter().onRegister(etUsernameEnroll.getText().toString(), etPasswordEnroll.getText().toString(), etEmailEnroll.getText().toString());
                }
            }
        });
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation clockwise = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_left_to_right);
                if (!isSigninScreen) {
                    btnSignup.startAnimation(clockwise);
                } else {
                    getPresenter().onAuthorization(etUsernameLogin.getText().toString(), etPassword.getText().toString());
                }
            }
        });

    }

    @Override
    protected AuthorizationPresenter getPresenter() {
        return presenter;
    }

    private void showSignupForm() {
        PercentRelativeLayout.LayoutParams paramsLogin = (PercentRelativeLayout.LayoutParams) llSignin.getLayoutParams();
        PercentLayoutHelper.PercentLayoutInfo infoLogin = paramsLogin.getPercentLayoutInfo();
        infoLogin.widthPercent = 0.15f;
        llSignin.requestLayout();


        PercentRelativeLayout.LayoutParams paramsSignup = (PercentRelativeLayout.LayoutParams) llSignup.getLayoutParams();
        PercentLayoutHelper.PercentLayoutInfo infoSignup = paramsSignup.getPercentLayoutInfo();
        infoSignup.widthPercent = 0.85f;
        llSignup.requestLayout();

        tvSignupInvoker.setVisibility(View.GONE);
        tvSigninInvoker.setVisibility(View.VISIBLE);
        Animation translate = AnimationUtils.loadAnimation(getContext(), R.anim.translate_right_to_left);
        llSignup.startAnimation(translate);

        Animation clockwise = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_right_to_left);
        btnSignup.startAnimation(clockwise);

    }

    private void showSigninForm() {
        PercentRelativeLayout.LayoutParams paramsLogin = (PercentRelativeLayout.LayoutParams) llSignin.getLayoutParams();
        PercentLayoutHelper.PercentLayoutInfo infoLogin = paramsLogin.getPercentLayoutInfo();
        infoLogin.widthPercent = 0.85f;
        llSignin.requestLayout();


        PercentRelativeLayout.LayoutParams paramsSignup = (PercentRelativeLayout.LayoutParams) llSignup.getLayoutParams();
        PercentLayoutHelper.PercentLayoutInfo infoSignup = paramsSignup.getPercentLayoutInfo();
        infoSignup.widthPercent = 0.15f;
        llSignup.requestLayout();

        Animation translate = AnimationUtils.loadAnimation(getContext(), R.anim.translate_left_to_right);
        llSignin.startAnimation(translate);

        tvSignupInvoker.setVisibility(View.VISIBLE);
        tvSigninInvoker.setVisibility(View.GONE);
        Animation clockwise = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_left_to_right);
        btnSignin.startAnimation(clockwise);
    }
}
