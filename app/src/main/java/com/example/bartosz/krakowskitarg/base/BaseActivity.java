package com.example.bartosz.krakowskitarg.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.bartosz.krakowskitarg.authorization.AuthorizationActivity;
import com.example.bartosz.krakowskitarg.base.util.Util;

import butterknife.ButterKnife;
import timber.log.Timber;

public abstract class BaseActivity<PRESENTER extends Presenter<VIEW>, VIEW extends BaseView>
        extends AppCompatActivity implements BaseView {

    protected static final String ERROR_CODE_MESSAGE = BaseActivity.class.getName() + "_error_code_key";

    protected abstract PRESENTER getPresenter();

    @Override
    public String getStringFromId(int id) {
        return getString(id);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().onViewDestroyed();
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getPresenter().onViewCreated((VIEW) this);
        initInjection();
    }

    protected abstract void initInjection();

    @Override
    public void showDialog(String message) {
        Util.showDialog(getActivity(), message);
    }

  @Override public void logoutUser(String errorMessage) {
    Intent intent = new Intent(this, AuthorizationActivity.class);
    intent.putExtra(ERROR_CODE_MESSAGE, errorMessage);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(intent);
  }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void log(String message) {
        Timber.d(message);
    }
}