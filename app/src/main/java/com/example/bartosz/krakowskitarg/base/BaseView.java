package com.example.bartosz.krakowskitarg.base;

import android.app.Activity;

/**
 * Created by Bartosz on 01.04.2017.
 */

public interface BaseView {
    void showDialog(String name);

    void showToast(String name);

    String getStringFromId(int id);

    void log(String message);

    Activity getActivity();

    void logoutUser(String errorMessage);
}
