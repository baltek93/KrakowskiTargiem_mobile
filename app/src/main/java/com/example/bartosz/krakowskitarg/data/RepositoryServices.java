package com.example.bartosz.krakowskitarg.data;

import com.example.bartosz.krakowskitarg.base.AuthorizationToken;
import com.example.bartosz.krakowskitarg.base.LocalRepositoryInterface;
import com.example.bartosz.krakowskitarg.data.model.LoginRequest;
import com.example.bartosz.krakowskitarg.data.model.Offer;

import java.util.List;

import retrofit2.Call;

/**
 * Created by Bartosz on 01.04.2017.
 */

public class RepositoryServices implements Repository {
    RemoteRepositoryInterface remoteRepositoryInterface;
    LocalRepositoryInterface localRepositoryInterface;

    public RepositoryServices(RemoteRepositoryInterface remoteRepositoryInterface, LocalRepositoryInterface localRepositoryInterface) {
        this.remoteRepositoryInterface = remoteRepositoryInterface;
        this.localRepositoryInterface=localRepositoryInterface;
    }


    @Override
    public Call<AuthorizationToken> authorization(LoginRequest loginRequest) {
        return remoteRepositoryInterface.authorization(loginRequest);
    }

    @Override
    public Call<List<Offer>> getOffer(String authorization) {
        return remoteRepositoryInterface.getOffer(authorization);

    }



//
//    @Override
//    public Observable<GithubUser> getUser(@Path("username") String username) {
//        return null;
//    }
}
