package com.example.bartosz.krakowskitarg.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Bartosz on 09.04.2017.
 */
@Getter @Setter @Builder @AllArgsConstructor
public class LoginRequest {


    private String username;
    private String password;


}
