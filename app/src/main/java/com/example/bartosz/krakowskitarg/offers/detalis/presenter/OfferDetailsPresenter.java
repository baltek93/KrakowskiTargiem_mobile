package com.example.bartosz.krakowskitarg.offers.detalis.presenter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.bartosz.krakowskitarg.R;
import com.example.bartosz.krakowskitarg.base.Presenter;
import com.example.bartosz.krakowskitarg.offers.detalis.usecase.OfferDetailsUseCase;
import com.example.bartosz.krakowskitarg.offers.detalis.view.OfferDetailsView;
import com.example.bartosz.krakowskitarg.offers.main.view.OfferFragment;


public class OfferDetailsPresenter extends Presenter<OfferDetailsView> {

    public OfferDetailsPresenter(OfferDetailsUseCase mUseCase) {
        super(mUseCase);
    }

}
