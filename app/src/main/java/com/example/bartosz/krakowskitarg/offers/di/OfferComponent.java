package com.example.bartosz.krakowskitarg.offers.di;

import android.support.annotation.NonNull;

import com.example.bartosz.krakowskitarg.HomeActivityScope;
import com.example.bartosz.krakowskitarg.MyApplicationComponent;
import com.example.bartosz.krakowskitarg.addoffer.view.AddOfferFragment;
import com.example.bartosz.krakowskitarg.offers.detalis.view.OfferDetailsFragment;
import com.example.bartosz.krakowskitarg.offers.main.view.OfferFragment;

import dagger.Component;

/**
 * Created by Bartosz on 24.04.2017.
 */
@HomeActivityScope
@Component(modules = OfferModule.class, dependencies = MyApplicationComponent.class)
public interface OfferComponent {
    void inject(@NonNull OfferFragment fragment);
    void inject(@NonNull OfferDetailsFragment fragment);
    void inject(@NonNull AddOfferFragment fragment);
}
