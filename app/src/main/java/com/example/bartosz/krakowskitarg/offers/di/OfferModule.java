package com.example.bartosz.krakowskitarg.offers.di;

import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.HomeActivityScope;
import com.example.bartosz.krakowskitarg.addoffer.presenter.AddOfferPresenter;
import com.example.bartosz.krakowskitarg.addoffer.usecase.AddOfferUseCase;
import com.example.bartosz.krakowskitarg.addoffer.view.AddOfferView;
import com.example.bartosz.krakowskitarg.data.Repository;

import com.example.bartosz.krakowskitarg.offers.detalis.presenter.OfferDetailsPresenter;
import com.example.bartosz.krakowskitarg.offers.detalis.usecase.OfferDetailsUseCase;
import com.example.bartosz.krakowskitarg.offers.main.presenter.OfferPresenter;
import com.example.bartosz.krakowskitarg.offers.main.usecase.OfferUseCase;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Bartosz on 24.04.2017.
 */
@Module
public class OfferModule {
    @Provides
    @HomeActivityScope
    public OfferUseCase providesOfferUseCase(Repository repository, Retrofit retrofit, SharedPreferences sharedPreferences) {
        return new OfferUseCase(repository,retrofit,sharedPreferences);
    }
    @Provides
    @HomeActivityScope
    public OfferPresenter providesOfferPresenter(OfferUseCase usecase) {
        return new OfferPresenter(usecase);
    }

    @Provides
    @HomeActivityScope
    public OfferDetailsUseCase providesOfferDetailsUseCase(Repository repository, Retrofit retrofit, SharedPreferences sharedPreferences) {
        return new OfferDetailsUseCase(repository,retrofit,sharedPreferences);
    }
    @Provides
    @HomeActivityScope
    public OfferDetailsPresenter providesOfferDetailsPresenter(OfferDetailsUseCase usecase) {
        return new OfferDetailsPresenter(usecase);
    }

    @Provides
    @HomeActivityScope
    public AddOfferUseCase providesAddOfferUseCase(Repository repository, Retrofit retrofit, SharedPreferences sharedPreferences) {
        return new AddOfferUseCase(repository,retrofit,sharedPreferences);
    }
    @Provides
    @HomeActivityScope
    public AddOfferPresenter providesAddOfferresenter(AddOfferUseCase usecase) {
        return new AddOfferPresenter(usecase);
    }
}
