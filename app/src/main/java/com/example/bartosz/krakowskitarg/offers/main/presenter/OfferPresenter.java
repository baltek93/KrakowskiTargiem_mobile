package com.example.bartosz.krakowskitarg.offers.main.presenter;

import com.example.bartosz.krakowskitarg.data.model.Offer;
import com.example.bartosz.krakowskitarg.offers.main.usecase.OfferUseCase;
import com.example.bartosz.krakowskitarg.offers.main.view.OfferView;
import com.example.bartosz.krakowskitarg.base.Presenter;

import java.util.List;

/**
 * Created by Bartosz on 24.04.2017.
 */

public class OfferPresenter extends Presenter<OfferView> implements OfferUseCase.OfferCallback {
    OfferUseCase mUseCase;

    public OfferPresenter(OfferUseCase mUseCase) {
        super(mUseCase);
        this.mUseCase = mUseCase;
    }

    @Override
    public void onViewVisible() {
        super.onViewVisible();
        mUseCase.getOffer(this);

    }

    @Override
    public void renderOffer(List<Offer> offerList) {
        if (offerList != null && offerList.size() > 0) {
            getView().showToast("Są offerty");
            getView().renderOffer(offerList);
        } else {
            getView().showToast("Nie ma offerty");

        }

    }
}
