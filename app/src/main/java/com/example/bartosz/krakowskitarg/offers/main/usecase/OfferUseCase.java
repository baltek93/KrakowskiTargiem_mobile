package com.example.bartosz.krakowskitarg.offers.main.usecase;

import android.content.SharedPreferences;

import com.example.bartosz.krakowskitarg.base.UseCase;
import com.example.bartosz.krakowskitarg.data.Repository;

import com.example.bartosz.krakowskitarg.data.model.Offer;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

/**
 * Created by Bartosz on 24.04.2017.
 */

public class OfferUseCase extends UseCase {
  public OfferUseCase(Repository mRepository, Retrofit retrofit,
      SharedPreferences sharedPreferences) {
    super(mRepository, retrofit, sharedPreferences);
  }

  public void getOffer(OfferCallback authorizationCallback) {
    mRemoteRepositoryInterface.getOffer(getSharedPreferences().getString("TOKEN","")).enqueue(new Callback<List<Offer>>() {
      @Override public void onResponse(Call<List<Offer>> call, Response<List<Offer>> response) {
        Timber.e(response.body().get(0).getName());
        authorizationCallback.renderOffer(response.body());
      }

      @Override public void onFailure(Call<List<Offer>> call, Throwable t) {
        Timber.e(t.getMessage());

      }
    });
  }

  public interface OfferCallback {
    void renderOffer(List<Offer> offerList);
  }
}
